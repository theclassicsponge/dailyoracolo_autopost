import time
import json
import requests
from defines_for_post import getCreds, makeApiCall
from soup import geturl, getdata


def createMediaObject(params):
    """ Create media object
    Args:
    	params: dictionary of params

    API Endpoint:
    	https://graph.facebook.com/v5.0/{ig-user-id}/media?image_url={image-url}&caption={caption}&access_token={access-token}
    	https://graph.facebook.com/v5.0/{ig-user-id}/media?video_url={video-url}&caption={caption}&access_token={access-token}
    Returns:
    	object: data from the endpoint
    """

    url = params['endpoint_base'] + params['instagram_account_id'] + '/media'

    endpointParams = dict()
    endpointParams['caption'] = params['caption']
    endpointParams['access_token'] = params['access_token']

    if 'IMAGE' == params['media_type']:
        endpointParams['image_url'] = params['media_url']
    else:
        endpointParams['media_type'] = params['media_type']
        endpointParams['video_url'] = params['media_url']

    return makeApiCall(url, endpointParams, 'POST')




def getMediaObjectStatus(mediaObjectId, params):
    url = params['endpoint_base'] + '/' + mediaObjectId

    endpointParams = dict()
    endpointParams['fields'] = 'status_code'
    endpointParams['access_token'] = params['access_token']

    return makeApiCall(url, endpointParams, 'GET')


def publishMedia(mediaObjectId, params):
    url = params['endpoint_base'] + params['instagram_account_id'] + '/media_publish'

    endpointParams = dict()
    endpointParams['creation_id'] = mediaObjectId
    endpointParams['access_token'] = params['access_token']

    return makeApiCall(url, endpointParams, 'POST')

params = getCreds()

params['media_type'] = 'IMAGE'
params['media_url'] = f'{geturl()}'
params['caption'] = 'This is a test.'
params['caption'] += "\n."
params['caption'] += "\n---------------------------------------------------------------------"
params['caption'] += "\n."
params['caption'] += "\n"  # caption for the post

imageMediaObjectResponse = createMediaObject(params)  # create a media object through the api
imageMediaObjectId = imageMediaObjectResponse['json_data']['id']  # id of the media object that was created
imageMediaStatusCode = 'IN_PROGRESS';

print("\n---- IMAGE MEDIA OBJECT -----\n") # title
print("\tID:") # label
print("\t" + imageMediaObjectId) # id of the object

while imageMediaStatusCode != 'FINISHED':
    imageMediaObjectStatusResponse = getMediaObjectStatus(imageMediaObjectId, params)
    imageMediaStatusCode = imageMediaObjectStatusResponse['json_data']['status_code']

    print("\n---- IMAGE MEDIA OBJECT STATUS -----\n")  # display status response
    print("\tStatus Code:")  # label
    print("\t" + imageMediaStatusCode)  # status code of the object

    time.sleep(5)

publishImageResponse = publishMedia(imageMediaObjectId, params)

print( "\n---- PUBLISHED IMAGE RESPONSE -----\n" ) # title
print( "\tResponse:" ) # label
print( publishImageResponse['json_data_pretty'] ) # json response from ig api