import requests
import time
import configparser

from bs4 import BeautifulSoup

config = configparser.ConfigParser()
config.read('keys.ini')


def getdata(url):
    r = requests.get(url)
    return r.text


def geturl():
    htmldata = getdata(f'{config["URL"]["Image_url"]}')
    soup = BeautifulSoup(htmldata, 'html.parser')
    # srcs = [img['src'] for img in soup.find_all('img')]
    # srcs
    for item in soup.find_all('img'):
        print(str(item['src']))
        time.sleep(int(f'{config["TIME_TO_WAIT"]["Time"]}'))


geturl()


