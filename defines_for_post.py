import requests
import json
import configparser

config = configparser.ConfigParser()
config.read('keys.ini')

def getCreds():
    creds = dict()
    creds['access_token'] = f'{config["KEYS"]["Access_token"]}'
    creds['graph_domain'] = 'https://graph.facebook.com/'
    creds['graph_version'] = 'v6.0'
    creds['endpoint_base'] = creds['graph_domain'] + creds['graph_version'] + '/'
    creds['instagram_account_id'] = f'{config["KEYS"]["Account_id"]}'

    return creds


def makeApiCall(url, endpointParams, type):
    if type == 'POST' : # post request
        data = requests.post(url, endpointParams)
    else:
        data = requests.get(url, endpointParams)

    response = dict()
    response['url'] = url
    response['endpoint_params'] = endpointParams
    response['endpoint_params_pretty'] = json.dumps(endpointParams, indent = 4)
    response['json_data'] = json.loads(data.content)
    response['json_data_pretty'] = json.dumps(response['json_data'], indent = 4)

    return response
